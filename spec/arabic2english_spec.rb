RSpec.describe "humanize"  do


context "number is an integer" do
it "convert 1 from arabic number to english number"  do
expect(%x( ruby arabic2english.rb 1 )).to eq 'one'
end

it "convert 11 from arabic number to english number"  do
expect(%x( ruby arabic2english.rb 11 )).to eq 'eleven'
end

it "convert 100 from arabic number to english number"  do
expect(%x( ruby arabic2english.rb 100 )).to eq 'one hundred'
end

end
context "number is a string" do
it "convert 1 from arabic number to english number"  do
expect(%x( ruby arabic2english.rb '1' )).to eq 'one'
end
end
end
